//
//  ContentView.swift
//  myApp
//
//  Created by zhangxuan on 2022/7/19.
//

import SwiftUI
import Alamofire

//func request<T: Decodable>(url: URL, callback: @escaping (DataResponse<T, AFError>) -> Void) {
//    AF.request(url).responseDecodable { (response: DataResponse<T, AFError>) in
//        callback(response)
//    }
//}

//struct ContentView: View {
//    var body: some View {
//        Text("测试!")
//            .padding()
//    }
//}

// struct 表示结构体，如果以前没接触过，你暂时把它理解成 class 类就好了
// (实际上结构体和类有本质区别，这里不展开讲)
//struct ContentView: View {
//  // @State 修饰的变量表示本地状态
//  // todoList 的任何改变，都会自动触发 UI 的重新渲染
//  @State private var todoList = ["Apple", "Pear", "Tomato"]
//
//  var body: some View {
//    // 列表控件
//    List {
//      // 循环渲染元素
//      ForEach(todoList, id: \.self) { item in
//        Text(item)
//      }
//    }
//  }
//}

//struct MainToDoItem: Identifiable {
//    let id    = UUID()
//    var isOn  = true
//    let name: String
//}
struct HTTPBinResponse: Decodable {
    let url: String
    let msg: String
}
class rr: Decodable {
    // 如果你希望自己的模型仅仅只是接受网络请求获取的数据,不做任何改变,那么建议使用let 而不是使用var
    let msg: String?
}
struct MainView: View {
////    var myAppApp
//    // 装饰器 @State 表示 todoList 是一个受监控的本地状态
//    @State private var todoList = [
//        MainToDoItem(name: "Apple"),
//        MainToDoItem(name: "Pear"),
//        MainToDoItem(name: "Tomato")
//    ]
//    // 新增本地状态 newName
//    // 用于接收用户输入的文本
//    @State private var newName: String = ""
//
////    var ListView = ListView()

  
    var body: some View {
        NavigationView {
            VStack {
                
                NavigationLink(destination: ListView()) {
                    Text("跳转列表")
                }
                .navigationBarTitle("")
                
                Button(action: {
                    print("wswe")
                    Alamofire.AF.request("https://api.spjgzs.com/api/code/commodity/list",method: .post, parameters: ["num":"0"])
                        .responseDecodable(of: rr.self) { response in
                            print(response)
//                            debugPrint("Response: \(response)")
                        }
//                        .responseJSON { (response) in
//                            print(response.value)   // result of response
//                    }
                    
//                    Alamofire.AF.request("https://api.spjgzs.com/api/code/commodity/searchCode",method: .post, parameters: ["code":"6903473100014"])
//                        .responseJSON { (response) in
//                            print(response.value)   // result of response
//                    }
                        

//                    let urlStr = "http://onapp.yahibo.top/public/?s=api/test/list"
//                    AF.request(urlStr, method: .post, parameters: ["name":"hibo","password":"123456"]).responseDecodable { (response) in
//                        switch response.result {
//                        case .success(let json):
//                            print(json)
//                            break
//                        case .failure(let error):
//                            print("error:\(error)")
//                            break
//                        }
//                    }
//                    AF.request("https://api.spjgzs.com/api/code/commodity/searchCode").response{
//                        (request) in
//                        print(request)
//                    }
//                    Alamofire.Request("https://api.spjgzs.com/api/code/commodity/searchCode").response{}
//                    Alamofire .Request("https://api.spjgzs.com/api/code/commodity/searchCode").responseJSON { (response) in
//
//                            print(response.request)  // original URL request
//                            print(response.response) // URL response
//                            print(response.data)     // server data
//                            print(response.result)   // result of response serialization
//
//                            if let JSON = response.result.value {
//                                print("JSON: \(JSON)")
//                            }
//                        }
                    
//                    Alamofire.request(.GET, "https://httpbin.org/get", parameters: ["foo": "bar"])
//                        .responseJSON { response in
//                            print(response.request)  // original URL request
//                            print(response.response) // URL response
//                            print(response.data)     // server data
//                            print(response.result)   // result of response serialization
//
//                            if let JSON = response.result.value {
//                                print("JSON: \(JSON)")
//                            }
//                    }

//                    guard let url = URL(string: "https://api.spjgzs.com/api/code/commodity/searchCode") else {
//                               return
//                           }
//                    var urlRequest = URLRequest(url: url)
//                    urlRequest.httpMethod = "POST"
//
//                    let config = URLSessionConfiguration.default
//                    config.httpAdditionalHeaders = ["Content-Type":"application/json"]
//                    config.timeoutIntervalForRequest = 30
//                    config.requestCachePolicy = .reloadIgnoringLocalCacheData
//                    let session = URLSession(configuration: config)
//
//                    session.dataTask(with: urlRequest){
//                        (data,_,_) in
//                        if let resultData = data{
////                            print(data)
//                            do {
//                                let jsonObject = try JSONSerialization.jsonObject(with: resultData, options:[.mutableContainers,.mutableLeaves])
//
//                                print(jsonObject)
//                            }catch{
//                                print("error")
//                            }
//                        }
//                    }.resume()
//                    print(urlRequest)

                }){
                    Text("测试")
                }
                .padding(10)
                
            }
        }
//              Button(action: {
////                  newName = "叔叔说"
//                  print("wsw")
//              }){
//                  Text("测试")
//              }
//        }

      // VStack: 垂直方向布局
//      VStack {
//          // 列表元素
//          // HStack: 水平方向布局
//          HStack {
//            // 文本控件
//            TextField("输入新事项", text: $newName)
//            // 按钮控件
//            Button("确认") {
//              // 点击按钮后执行的操作
//              let newItem = ToDoItem(name: newName)
//              todoList.append(newItem)
//              newName = ""
//            }
//          }.padding()
//          Button(action: {
//              newName = "叔叔说"
//              print("wsw")
////              NavigationLink(destination:listView) {
////
////              }
//          }){
//              Text("测试")
//          }
//
//          List {
//            // 动态遍历渲染
//            ForEach(todoList) { item in
//                Text(item.name)
//            }
//          }
//        }
    }
}

struct Main_ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
