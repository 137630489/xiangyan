//
//  ContentView.swift
//  myApp
//
//  Created by zhangxuan on 2022/7/19.
//

import SwiftUI

//struct ContentView: View {
//    var body: some View {
//        Text("测试!")
//            .padding()
//    }
//}

// struct 表示结构体，如果以前没接触过，你暂时把它理解成 class 类就好了
// (实际上结构体和类有本质区别，这里不展开讲)
//struct ContentView: View {
//  // @State 修饰的变量表示本地状态
//  // todoList 的任何改变，都会自动触发 UI 的重新渲染
//  @State private var todoList = ["Apple", "Pear", "Tomato"]
//
//  var body: some View {
//    // 列表控件
//    List {
//      // 循环渲染元素
//      ForEach(todoList, id: \.self) { item in
//        Text(item)
//      }
//    }
//  }
//}

struct ListToDoItem: Identifiable {
    let id    = UUID()
    var isOn  = true
    let name: String
}
struct ListView: View {
//    var myAppApp
    // 装饰器 @State 表示 todoList 是一个受监控的本地状态
    @State private var todoList = [
        ListToDoItem(name: "Apple"),
        ListToDoItem(name: "Pear"),
        ListToDoItem(name: "Tomato")
    ]
    // 新增本地状态 newName
    // 用于接收用户输入的文本
    @State private var newName: String = ""
  
    var body: some View {
      // VStack: 垂直方向布局
      VStack {
          // 列表元素
          // HStack: 水平方向布局
//          HStack {
//            // 文本控件
//            TextField("输入新事项", text: $newName)
//            // 按钮控件
//            Button("确认") {
//              // 点击按钮后执行的操作
//              let newItem = ToDoItem(name: newName)
//              todoList.append(newItem)
//              newName = ""
//            }
//          }.padding()
//          Button("测试") {
//              NavigationLink(destination:SearchView(searchText: searchText), isActive: $isCommit) {
//
//          }
          List {
            // 动态遍历渲染
            ForEach(todoList) { item in
                Text(item.name)
            }
          }
        }
    }
}

struct ContentView_Previews_List: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
